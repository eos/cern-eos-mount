# Packages to directly mount CERN user/project and misc. instances.

It provides sveral RPMS to configure/start AUTOFS or Systemd automounts.

## Quick Configuration

### AUTOFS
* if you want all CERN mounts with autofs enabled, install cern-eos-autofs - this also supports OAUTH2 authorization by default and the token file has to be defined in the environment via OAUTH2_TOKEN=path-to-file. Make sure autofs is running after installation: systemctl start autofs

### SYSTEMD
* if you want all CERN mounts with systemd autmount enabled, install cern-eos-systemd-mountit.
* if you want all CERN mounts with systemd automount enabled but with reduced memory footprint, install cern-eos-systemd-bind-mountit.

Warning: we recommend to use AUTOFS because the systemd implementation sometimes triggers two mounts.

If you want to select which mounts you want to have enabled, install cern-eos-systemd or cern-eos-systemd-bind and enable/start each automount target manually:

```bash
# e.g. from PUPPET
systemctl enable eos-cms.automount
systemctl start eos-cms.automount

# or to disable at a later stage
# systemctl disable eos-cms.automount
# systemctl stop eos-cms.automount

```

## RPM cern-eos-autofs


AUTOFS based configuration of all CERN mounts using krb5/gsi and OAUTH2 authentication.
Requires to have AUTOFS running: systemctl start autofs

## RPM cern-eos-autofs-lxplus

AUTOFS based configuration of all CERN mounts using krb5/gsi and OAUTH2 authentication.
Requires to have AUTOFS running: systemctl start autofs

## RPM cern-eos-autofs-samba

AUTOFS based configuration for SAMBA gateways - for gateway machines without krb5 authentication.
Requires to have AUTOFS running: systemctl start autofs

## RPM cern-eos-autofs-squashfs

AUTOFS based configuration for SquashFS images on EOS mounts. 
Requires to have AUTOFS running: systemctl start autofs

## RPM cern-eos-systemd

Systemd autmount configuration for all CERN mounts using krb5 authentication.
This package does not enable or start autmounts.

## RPM cern-eos-systemd-core

Config file package for all CERN mounts for systemd automounts.

## RPM cern-eos-systemd-mountit

This package enables/starts all autmounts. Install it to get all CERN mounts with krb5 authentication on the target machine.

## RPM cern-eos-systemd-bind

Systemd autmount configuration for all CERN mounts using krb5 authentication. This package does not provide 26 letters mounts for user/projects mounts, it mounts all home/project instances as well as eosmedia and eospublic to top-level mountpoints outside `/eos`, and bind mounts letters to the corresponding mounts within an `/eos` tree. Use it, where memory footprint is an issue, or where you want to limit contention.

This package does not enable or start autmounts.

## RPM cern-eos-systemd-bind-mountit

This package enables/starts all autmounts from the systemd-bind package. Install it to get all CERN mounts with krb5 authentication on the target machine a reduced memory footprint.

## RPM cern-eos-systemd-lxplus

The Systemd automount configuration for lxplus using 26 letter mounts and krb5 authentication.

## RPM cern-eos-systemd-samba

The Systemd SAMBA autmount configuration for all CERN mounts for gateway machines without krb5 authentication.

## RPM cern-eos-systemd-samba-bind

This package uses the same logic as `cern-eos-systemd-bind`, but bind-mounts all user-facing mounts to a tree rooted at `/samba`. The configuration is suitable for gateway machines without krb5 authentication.
