#!/usr/bin/env bash
set -e

#-------------------------------------------------------------------------------
# Generate a release tarball - run this from the root of the git repository.
#-------------------------------------------------------------------------------
git submodule update --recursive --init
git fetch --tags

mkdir -p build
./genversion.py --template cern-eos-mount.spec.in --out cern-eos-mount.spec

#-------------------------------------------------------------------------------
# Extract version number, we need this for the archive name
#-------------------------------------------------------------------------------
VERSION_FULL=$(./genversion.py --template-string "@VERSION_FULL@")
printf "Version: ${VERSION_FULL}\n"
FILENAME="cern-eos-mount-${VERSION_FULL}"

#-------------------------------------------------------------------------------
# Make the archive
#-------------------------------------------------------------------------------
TARGET_PATH=$(basename "$PWD")

pushd "$PWD"/..
tar --exclude '*/.git' --exclude "${TARGET_PATH}/build" --exclude '.gitlab-ci.yml' --exclude '.gitignore' -pcvzf "${TARGET_PATH}/build/${FILENAME}.tar.gz" "${TARGET_PATH}" --transform "s!^${TARGET_PATH}!${FILENAME}!" --show-transformed-names
popd
