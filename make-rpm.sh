#!/usr/bin/env bash

git submodule update --init --recursive
./make-srpm.sh
cd build

if which dnf; then
  dnf builddep -y ~/rpmbuild/SRPMS/*
else
  yum-builddep -y ~/rpmbuild/SRPMS/*
fi

rpmbuild --rebuild --with server --define "_build_name_fmt %%{NAME}-%%{VERSION}-%%{RELEASE}.%%{ARCH}.rpm" ~/rpmbuild/SRPMS/*
