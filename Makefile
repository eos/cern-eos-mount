.PHONY: all disk srpm rpm 

all:	
	echo "run: make dist|srpm|rpm"

clean: 
	rm -rf build/
	test -e cern-eos-mount.spec && unlink cern-eos-mount.spec || echo 

dist:   clean
	./make-dist.sh

srpm:	dist
	./make-srpm.sh

rpm:	srpm
	./make-rpm.sh

