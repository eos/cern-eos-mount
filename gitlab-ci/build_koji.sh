#!/usr/bin/env bash


kinit stci@CERN.CH -k -t /stci.krb5/stci.keytab

KOJI_SCRATCH=${1-}

build_koji() {   # args: artifacts targetdist
    echo "Building ${KOJI_SCRATCH} in KOJI from ${1} sources for target ${2}"
    koji build ${KOJI_SCRATCH} ${2} ${1}/SRPMS/*.src.rpm

}

build_koji el-7_artifacts eos7
build_koji el-8_artifacts eos8al
build_koji el-9_artifacts eos9al

# special case for rhel, where alma SRPM artifacts are used to trigger the build
build_koji el-8_artifacts eos8el
build_koji el-9_artifacts eos9el
