#!/usr/bin/env bash


export_rpms() {   # args: artifacts targetdist
    EXPORT_DIR=${STCI_ROOT_PATH}/${EOS_CODENAME}-depend/${2}
    echo "Publishing from $1 in location: ${EXPORT_DIR}"
    mkdir -p ${EXPORT_DIR}/SRPM
    cp -n ${1}/SRPMS/*.src.rpm ${EXPORT_DIR}/SRPM
    mkdir -p ${EXPORT_DIR}/x86_64
    cp -n ${1}/RPMS/*.rpm ${EXPORT_DIR}/x86_64
    createrepo -q ${EXPORT_DIR}/x86_64
}


export STCI_ROOT_PATH="/eos/project/s/storage-ci/www/eos"
echo "Exporting STCI_ROOT_PATH=${STCI_ROOT_PATH}"
export EOS_CODENAME="diopside"
echo "Exporting EOS_CODENAME=${EOS_CODENAME}"


kinit stci@CERN.CH -k -t /stci.krb5/stci.keytab


tree el-7_artifacts el-8_artifacts el-9_artifacts

export_rpms el-7_artifacts el-7
export_rpms el-8_artifacts el-8
export_rpms el-9_artifacts el-9

